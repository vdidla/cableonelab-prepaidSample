package com.user.prePaidDomainModel.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.user.prePaidDomainModel.domain.request.CustomerRequest;
import com.user.prePaidDomainModel.domain.request.SignInRequest;
import com.user.prePaidDomainModel.entity.Account;
import com.user.prePaidDomainModel.entity.Customer;
import com.user.prePaidDomainModel.repository.AccountPaymentRepository;

@Component
public class PrepaidService {

private AccountPaymentRepository repository;
	
	@Autowired
	public PrepaidService(AccountPaymentRepository repository) {
		this.repository = repository;
	}
	
	public void associateRegistration(CustomerRequest request){
		Account account= new Account();
		account.setType("Pre-paid Service");
		account.setContact(request.getPhoneNum());
		account.setEmail(request.getEmailAddress());
		
		Customer customer=new Customer();
		customer.setAddress1(request.getAddress1());
		customer.setAddress2(request.getAddress2());
		customer.setAddressId(request.getAddressId());
		customer.setCity(request.getCity());
		customer.setEmailAddress(request.getEmailAddress());
		customer.setExistingCreditcard(false);
		customer.setFirstName(request.getFirstName());
		customer.setInstallationType(request.getInstallationType());
		customer.setLastName(request.getLastName());
		customer.setMigratePostpaid(false);
		customer.setOrderd(request.getOrderd());
		customer.setPhoneNum(request.getPhoneNum());
		customer.setZip(request.getZip());
		customer.setState(request.getState());
		customer.setPin(request.getPin());
		customer.setPhoneVerified(false);
		
		account.setCustomer(customer);

		repository.save(account);
	}
	
	public boolean loginCustomer(SignInRequest signInForm) {
		boolean loginSuccess=false;
		return loginSuccess;
	}
	
	public Account getCustomerDetails(long customerNumber) {
		return repository.findById(customerNumber).get();
	}
	
	
}
