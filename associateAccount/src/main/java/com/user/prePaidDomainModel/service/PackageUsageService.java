package com.user.prePaidDomainModel.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.user.prePaidDomainModel.domain.request.PackageUsageRequest;
import com.user.prePaidDomainModel.entity.Customer;
import com.user.prePaidDomainModel.entity.PackageUsage;
import com.user.prePaidDomainModel.repository.CustomerRepository;
import com.user.prePaidDomainModel.repository.PackageUsageRepository;

public class PackageUsageService {

	private PackageUsageRepository usageRepository;
	private CustomerRepository customerRepository;
	
	@Autowired
	public PackageUsageService(PackageUsageRepository usageRepository, CustomerRepository customerRepository) {
		this.usageRepository = usageRepository;
		this.customerRepository=customerRepository;
	}

	public void sendNotification(PackageUsageRequest request) {
		PackageUsage usage=new PackageUsage();
		usage.setCreatedAt(new Date());
		usage.setBalance(request.getBalance());
		usage.setData(request.getData());
		usage.setTlm(new Date());
		usage.setUpdatedAt(new Date());
		Optional<Customer> customer= customerRepository.findById(request.getCustomerId());
		if(customer != null) {
			usage.setCustomer(customer.get());	
		}
		usageRepository.save(usage);
	}

	public PackageUsage getUsages(long usageId) {
		Optional<PackageUsage> packageUsage= usageRepository.findById(usageId);
		if(packageUsage != null) {
			return packageUsage.get();
		}
		return null;
	}

	public List<PackageUsage> getUsagesByCustomerId(long customerId) {
		return usageRepository.findByCustomerId(customerId);
	}
	
}
