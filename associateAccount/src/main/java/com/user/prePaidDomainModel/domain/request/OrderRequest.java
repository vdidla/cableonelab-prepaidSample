package com.user.prePaidDomainModel.domain.request;

public class OrderRequest {
	private String packages;
	private int packageCount;
	private int totalPackages;
	private float taxPayedForPackage;
	private float grandTotal;

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public int getPackageCount() {
		return packageCount;
	}

	public void setPackageCount(int packageCount) {
		this.packageCount = packageCount;
	}

	public int getTotalPackages() {
		return totalPackages;
	}

	public void setTotalPackages(int totalPackages) {
		this.totalPackages = totalPackages;
	}

	public float getTaxPayedForPackage() {
		return taxPayedForPackage;
	}

	public void setTaxPayedForPackage(float taxPayedForPackage) {
		this.taxPayedForPackage = taxPayedForPackage;
	}

	public float getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(float grandTotal) {
		this.grandTotal = grandTotal;
	}



}
