package com.user.prePaidDomainModel.domain.request;

public class CustomerRequest {

	private String firstName;
	private String lastName;
	private long addressId;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String phoneNum;
	private boolean phoneVerified;
	private String emailAddress;
	private boolean existingCreditcard;
	private String orderd;
	private String installationType;
	private String pin;
	private boolean migratePostpaid;
	private OrderRequest order;
	private PaymentRequest payment;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getAddressId() {
		return addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public boolean isPhoneVerified() {
		return phoneVerified;
	}

	public void setPhoneVerified(boolean phoneVerified) {
		this.phoneVerified = phoneVerified;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isExistingCreditcard() {
		return existingCreditcard;
	}

	public void setExistingCreditcard(boolean existingCreditcard) {
		this.existingCreditcard = existingCreditcard;
	}

	public String getOrderd() {
		return orderd;
	}

	public void setOrderd(String orderd) {
		this.orderd = orderd;
	}

	public String getInstallationType() {
		return installationType;
	}

	public void setInstallationType(String installationType) {
		this.installationType = installationType;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public boolean isMigratePostpaid() {
		return migratePostpaid;
	}

	public void setMigratePostpaid(boolean migratePostpaid) {
		this.migratePostpaid = migratePostpaid;
	}

	public OrderRequest getOrder() {
		return order;
	}

	public void setOrder(OrderRequest order) {
		this.order = order;
	}

	public PaymentRequest getPayment() {
		return payment;
	}

	public void setPayment(PaymentRequest payment) {
		this.payment = payment;
	}

	@Override
	public String toString() {
		return "AssociateRequest [firstName=" + firstName + ", lastName=" + lastName + ", addressId=" + addressId
				+ ", address1=" + address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state + ", zip="
				+ zip + ", phoneNum=" + phoneNum + ", phoneVerified=" + phoneVerified + ", emailAddress=" + emailAddress
				+ ", existingCreditcard=" + existingCreditcard + ", orderd=" + orderd + ", installationType="
				+ installationType + ", pin=" + pin + ", migratePostpaid=" + migratePostpaid + ", order=" + order
				+ ", payment=" + payment + "]";
	}

}