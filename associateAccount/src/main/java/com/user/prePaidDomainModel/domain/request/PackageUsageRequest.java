package com.user.prePaidDomainModel.domain.request;

import java.util.Date;

public class PackageUsageRequest {

	private Long customerId;
	private Date tlm;
	private Double data;
	private Double balance;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Date getTlm() {
		return tlm;
	}

	public void setTlm(Date tlm) {
		this.tlm = tlm;
	}

	public Double getData() {
		return data;
	}

	public void setData(Double data) {
		this.data = data;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}
}
