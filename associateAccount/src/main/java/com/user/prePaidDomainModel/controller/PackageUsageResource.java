package com.user.prePaidDomainModel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.user.prePaidDomainModel.domain.request.PackageUsageRequest;
import com.user.prePaidDomainModel.entity.PackageUsage;
import com.user.prePaidDomainModel.service.PackageUsageService;

@RestController
@RequestMapping("/api/v1/packageUsage")
public class PackageUsageResource {

	private PackageUsageService usageService;

	@Autowired
	public PackageUsageResource(PackageUsageService usageService) {
		this.usageService = usageService;
	}

	@RequestMapping(value = "/addUsageEntry", method = RequestMethod.POST)
	public String addUsageEntry(@RequestBody PackageUsageRequest request) throws Exception {
		usageService.sendNotification(request);
		return "PackageUsage added Successful";
	}

	@RequestMapping(value = "/get/{usageId}", method = RequestMethod.GET)
	public PackageUsage getUsages(@PathVariable("usageId") long usageId)  {
		return usageService.getUsages(usageId);
	}
	
	@RequestMapping(value = "/get/customer/{customerId}", method = RequestMethod.GET)
	public List<PackageUsage> getUsagesByCustomerId(@PathVariable("customerId") long customerId)  {
		return usageService.getUsagesByCustomerId(customerId);
	}
	
	
}