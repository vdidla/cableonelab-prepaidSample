package com.user.prePaidDomainModel.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.user.prePaidDomainModel.domain.request.CustomerRequest;
import com.user.prePaidDomainModel.domain.request.SignInRequest;
import com.user.prePaidDomainModel.entity.Account;
import com.user.prePaidDomainModel.service.PrepaidService;



@RestController
@RequestMapping("/api/v1/prePaidSetting")
public class PrepaidResource {

	private PrepaidService prePaidService;

	@Autowired
	public PrepaidResource(PrepaidService prePaidService) {
		this.prePaidService = prePaidService;
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginAssociate(@RequestBody SignInRequest signInRequest) throws Exception {
		prePaidService.loginCustomer(signInRequest);
		return "Login Successful";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(@RequestBody CustomerRequest customer) throws Exception {
		prePaidService.associateRegistration(customer);
		return "Registration Successful";
	}

	@RequestMapping(value = "/get/{customerNumber}", method = RequestMethod.GET)
	public Account get(@PathVariable("customerNumber") long customerNumber)  {
		return prePaidService.getCustomerDetails(customerNumber);
	}
	
}