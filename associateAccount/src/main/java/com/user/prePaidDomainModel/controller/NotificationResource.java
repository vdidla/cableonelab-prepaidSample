package com.user.prePaidDomainModel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.user.prePaidDomainModel.domain.request.NotificationRequest;
import com.user.prePaidDomainModel.entity.Notification;
import com.user.prePaidDomainModel.service.NotificationService;

@RestController
@RequestMapping("/api/v1/notification")
public class NotificationResource {

	private NotificationService notificationService;

	@Autowired
	public NotificationResource(NotificationService notificationService) {
		this.notificationService = notificationService;
	}

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public String sendNotification(@RequestBody NotificationRequest request) throws Exception {
		notificationService.sendNotification(request);
		return "Notification sent Successful";
	}

	@RequestMapping(value = "/get/{notificationNumber}", method = RequestMethod.GET)
	public Notification getNotification(@PathVariable("associateNumber") long notificationId)  {
		return notificationService.getNotification(notificationId);
	}
	
	@RequestMapping(value = "/get/customer/{customerId}", method = RequestMethod.GET)
	public List<Notification> getNotificationsByCustomerId(@PathVariable("customerId") long customerId)  {
		return notificationService.getNotificationsByCustomer(customerId);
	}
	
	
}

