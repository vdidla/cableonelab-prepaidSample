package com.user.prePaidDomainModel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "packageOffer")
public class PackageOffer extends AuditModel {
    @Id
    @GeneratedValue(generator = "packageOffer_generator")
    @SequenceGenerator(
            name = "packageOffer_generator",
            sequenceName = "packageOffer_sequence",
            initialValue = 1000
    )
    private Long id;

    private String offerCode;
	@Column
	private Long value;
	@Column
	private String equipmentType;
	@Column
	private String serviceType;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOfferCode() {
		return offerCode;
	}
	public void setOfferCode(String offerCode) {
		this.offerCode = offerCode;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public String getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	
}
