package com.user.prePaidDomainModel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "packageUsage")
public class PackageUsage extends AuditModel {
    @Id
    @GeneratedValue(generator = "packageUsage_generator")
    @SequenceGenerator(
            name = "packageUsage_generator",
            sequenceName = "packageUsage_sequence",
            initialValue = 1000
    )
    private Long id;

    private Date tlm;
	@Column
	private Double data;
	@Column
	private Double balance;
	
	@OneToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getTlm() {
		return tlm;
	}

	public void setTlm(Date tlm) {
		this.tlm = tlm;
	}

	public Double getData() {
		return data;
	}

	public void setData(Double data) {
		this.data = data;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
}
