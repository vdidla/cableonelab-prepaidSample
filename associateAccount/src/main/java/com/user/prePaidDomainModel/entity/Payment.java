package com.user.prePaidDomainModel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "payment")
public class Payment extends AuditModel {
    @Id
    @GeneratedValue(generator = "payment_generator")
    @SequenceGenerator(
            name = "payment_generator",
            sequenceName = "payment_sequence",
            initialValue = 1000
    )
    private Long id;

    @Column
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Account account;
	
	@Column
	private String identityNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	};
	
	
}


