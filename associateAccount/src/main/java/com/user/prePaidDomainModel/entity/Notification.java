package com.user.prePaidDomainModel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "notification")
public class Notification extends AuditModel {
    @Id
    @GeneratedValue(generator = "notification_generator")
    @SequenceGenerator(
            name = "notification_generator",
            sequenceName = "notification_sequence",
            initialValue = 1000
    )
    private Long id;

   // @ManyToOne
	//@JoinColumn(name = "customer_Id")
	private Customer customer;
	@Column
	private String notificationType;
	@Column
	private String Contant;
	@Column
	private Date tlm;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getContant() {
		return Contant;
	}
	public void setContant(String contant) {
		Contant = contant;
	}
	public Date getTlm() {
		return tlm;
	}
	public void setTlm(Date tlm) {
		this.tlm = tlm;
	}

	
}
