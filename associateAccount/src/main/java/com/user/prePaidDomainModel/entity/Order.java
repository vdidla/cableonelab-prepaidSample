package com.user.prePaidDomainModel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "package_order")
public class Order extends AuditModel {
    @Id
    @GeneratedValue(generator = "order_generator")
    @SequenceGenerator(
            name = "order_generator",
            sequenceName = "order_sequence",
            initialValue = 1000
    )
    private Long id;

    private String packages;
	
	private Long packageCount;
	
	private Long totalPackages;
	
	private Double taxPayedForPackage;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPackages() {
		return packages;
	}
	public void setPackages(String packages) {
		this.packages = packages;
	}
	public Long getPackageCount() {
		return packageCount;
	}
	public void setPackageCount(Long packageCount) {
		this.packageCount = packageCount;
	}
	public Long getTotalPackages() {
		return totalPackages;
	}
	public void setTotalPackages(Long totalPackages) {
		this.totalPackages = totalPackages;
	}
	public Double getTaxPayedForPackage() {
		return taxPayedForPackage;
	}
	public void setTaxPayedForPackage(Double taxPayedForPackage) {
		this.taxPayedForPackage = taxPayedForPackage;
	}

	
	
}
