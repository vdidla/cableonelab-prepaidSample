package com.user.prePaidDomainModel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.prePaidDomainModel.entity.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long >{

}
