package com.user.prePaidDomainModel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.user.prePaidDomainModel.entity.Notification;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long >{

	List<Notification> findByCustomerId(long customerId);
}
