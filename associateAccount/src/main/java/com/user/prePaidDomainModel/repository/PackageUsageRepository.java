package com.user.prePaidDomainModel.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.prePaidDomainModel.entity.PackageUsage;

@Repository
public interface PackageUsageRepository extends CrudRepository<PackageUsage, Long >{

	List<PackageUsage> findByCustomerId(long customerId);
}
