package com.user.prePaidDomainModel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.user.prePaidDomainModel.entity.Payment;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long >{

}
